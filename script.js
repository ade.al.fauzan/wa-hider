$(document).ready(function () {
    blurText();
    setInterval(() => {
        blurText();
    }, 500)
    $(document).on('click', function () {
        blurText();
    })

    addHeader();
    let customBtn = '<a';
    customBtn += ' href="https://gitlab.com/ade.al.fauzan"';
    customBtn += ' target="_blank"';
    customBtn += ' style="';
    customBtn += ' display: block;';
    customBtn += ' position: fixed;';
    customBtn += ' background: rgba(0, 0, 0, 0.25);';
    customBtn += ' padding: 5px;';
    customBtn += ' border-radius: 25%;';
    customBtn += ' top: 70px;';
    customBtn += ' right: 10px;';
    customBtn += ' z-index: 999;';
    customBtn += '">Visit Me</a>';

    $('body').append(customBtn);
})

$('*[aria-label="Chat list"]>div').change(function () {
    console.log('ganti');
});

function addHeader() {
    let styleFormat = "<style>"
    styleFormat += ".blurtext { -webkit-filter: blur(8px); filter: blur(8px) } ";
    styleFormat += ".blurtext:hover { -webkit-filter: blur(0px); filter: blur(0px) } ";
    styleFormat += ".blurimage { -webkit-filter: brightness(5%) blur(8px); filter: brightness(5%) blur(8px);} ";
    styleFormat += ".blurimage:hover { -webkit-filter: brightness(100%) blur(0px); filter: brightness(100%) blur(0px); } ";
    styleFormat += ".blurthumb { -webkit-filter: blur(5px); -moz-filter: blur(5px); -o-filter: blur(5px); -ms-filter: blur(5px); filter: blur(5px); width: 100px; height: 100px; background-color: #ccc;}";
    styleFormat += "</style>";
    $(styleFormat).appendTo("head");
}

function blurText() {
    // SIDEBAR
    $('._ak8n').addClass('blurimage'); //CHAT IMAGE
    $('._ak8q').addClass('blurtext'); //CHAT TITLE
    $('._ak8i').addClass('blurtext'); //CHAT TIME
    $('._ak8k').addClass('blurtext'); //CHAT CONTENT
    $('._ahlk').addClass('blurtext'); //CHAT COUNT

    //MAIN CHAT
    $(getElementByXpath('//*[@id="main"]/header/div[1]/div')).addClass('blurimage'); //IMAGE 
    $(getElementByXpath('//*[@id="main"]/header/div[2]/div[1]')).addClass('blurtext'); //TITLE
    $(getElementByXpath('//*[@id="main"]/header/div[2]/div[2]')).addClass('blurtext'); //MEMBER
    $('*[role="row"]>div>div>div').addClass('blurimage'); //CONTENT
}

function getElementByXpath(path) {
    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}